<?php
header("Expires: Mon, 26 Jul 2011 05:00:00 GMT" ); 
header("Content-Type: text/xml");
include_once("lastRSS.php");
// 建立lastRSS物件
$rss = new lastRSS;
$rss->cache_dir = 'cache';  // 快取目錄
$rss->cache_time = 300;     // 快取間隔時間 
$rss->date_format = 'M d, Y g:i:s A';  // 日期格式
$rss->items_limit = 100;    // 最大項目數
$rss->CDATA = "content";    // 如何處理CDATA區塊
// RSS清單的結合陣列
$rssList = array(
  "News" => "http://tw.news.yahoo.com/rss/realtime",
  "Sports" => "http://tw.news.yahoo.com/rss/sports",
  "Heath" => "http://tw.news.yahoo.com/rss/health",
  "Finance" => "http://tw.news.yahoo.com/rss/finance"
);
// 選擇的RSS編號
$rssId = $_GET["id"];
// 取得RSS的URL網址
$url = $rssList[$rssId];
// 剖析RSS
$rs = $rss->get($url);
// 建立回應的XML文件
echo "<?xml version=\"1.0\" ?>\n";
echo "<channel>\n";
foreach ( $rs["items"] as $item) {
   echo "<item>\n";
   echo "<link>$item[link]</link>\n";
   echo "<title>$item[title]</title>\n";      
   echo "<pubDate>$item[pubDate]</pubDate>\n";
   echo "</item>\n";
}
echo "</channel>";
?>