// 建立XMLHttpRequest物件
function getHttpRequestObject(handler) {
   // 建立XMLHttpRequest物件
   var httpRequest = null;
   if ( window.XMLHttpRequest) {
      // IE7以上, Mozilla, Safari等瀏覽程式
      httpRequest = new XMLHttpRequest();
   } else if ( window.ActiveXObject ) { // IE5, IE6
      // 找出最新版MSXML剖析器
      var msxmls = [ "MSXML2.XMLHttp.4.0",
                     "MSXML2.XMLHttp.3.0",
                     "MSXML2.XMLHttp",
                     "Microsoft.XMLHttp"];
      for ( i = 0; i < msxmls.length; i++ ) {
         try {  // 建立XMLHttpRequest物件
            httpRequest = new ActiveXObject(msxmls[i]);
            break;
         } catch ( e ) {
            return null;
         }
      }
   }
   // 指定事件處理程序的名稱
   httpRequest.onreadystatechange = handler;
   return httpRequest;
}
// 開啟和送出非同步請求
function makeRequest(httpRequest, url) {
   httpRequest.open("GET", url, true); // 開啟
   httpRequest.send(null);             // 送出
}
var xmlHttp; var id;
window.setInterval("readRSS()", 1200000);  // 設定計時周期
// 送出HTTP請求來取得RSS的摘要資訊
function readRSS(value) {
   // 建立XMLHttpRequest物件
   xmlHttp = getHttpRequestObject(showRSSItem);
   if ( xmlHttp != null ) {
      var url = "readRSS.php";  // 建立URL網址
      if (value!=null) id=value; url=url+"?id="+id;
      makeRequest(xmlHttp, url); // 建立HTTP請求
   }
   else {
      alert ("錯誤! 瀏覽程式不支援XMLHttpRequest物件!");
      return;
   }   
}
// 顯示RSS的摘要資訊
function showRSSItem() {
   if ( xmlHttp.readyState == 4 && xmlHttp.status == 200 ) {
      // 取得回應RSS項目清單
      var xmlResult = xmlHttp.responseXML;
      var links = xmlResult.getElementsByTagName("link");
      var titles = xmlResult.getElementsByTagName("title");
      var pubDates = xmlResult.getElementsByTagName("pubDate");      
      var str = "<ul>";
      // 顯示所有XML節點
      for ( i = 0; i < links.length; i++ ) {
         var iLink = links[i].childNodes[0].nodeValue;         
         var iTitle = titles[i].childNodes[0].nodeValue;
         var iPubDate = pubDates[i].childNodes[0].nodeValue;
         str += "<li>( " + iPubDate + 
                "</font>) <a href='" + iLink + "'>" + 
                iTitle + "</a></li>";
      }
      document.getElementById("result").innerHTML = str + "</ul>";      
   }
}