<?php
header("Content-Type: text/xml");
// 延遲2秒
sleep(2);
echo "<?xml version=\"1.0\" ?>";
$doc = new DOMDocument();
$doc->load("Ch16_4.xml");
$books = $doc->getElementsByTagName("book");
echo "<titlelist>";
foreach( $books as $book ) {
  // 取出所有title元素
  $titles = $book->getElementsByTagName("title");
  $title = $titles->item(0)->nodeValue;
  echo "<title>" . $title . "</title>";
}  
echo "</titlelist>";
?>