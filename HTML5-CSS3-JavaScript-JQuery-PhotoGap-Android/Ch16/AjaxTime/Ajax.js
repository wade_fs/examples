// 建立XMLHttpRequest物件
function getHttpRequestObject(handler) {
   // 建立XMLHttpRequest物件
   var httpRequest = null;
   if ( window.XMLHttpRequest) {
      // IE7以上, Mozilla, Safari等瀏覽器
      httpRequest = new XMLHttpRequest();
   } else if ( window.ActiveXObject ) { // IE5, IE6
      // 找出最新版MSXML剖析器
      var msxmls = [ "MSXML2.XMLHttp.4.0",
                     "MSXML2.XMLHttp.3.0",
                     "MSXML2.XMLHttp",
                     "Microsoft.XMLHttp"];
      for ( i = 0; i < msxmls.length; i++ ) {
         try {  // 建立XMLHttpRequest物件
            httpRequest = new ActiveXObject(msxmls[i]);
            break;
         } catch ( e ) {
            return null;
         }
      }
   }
   // 指定事件處理函數的名稱
   httpRequest.onreadystatechange = handler;
   return httpRequest;
}
// 開啟和送出非同步請求
function makeRequest(httpRequest, url) {
   httpRequest.open("GET", url, true); // 開啟
   httpRequest.send(null);             // 送出
}
var xmlHttp;
// 送出HTTP請求來取得時間資訊
function getTime() {
   // 建立XMLHttpRequest物件
   xmlHttp = getHttpRequestObject(showTime);
   if ( xmlHttp != null ) {
      // 建立URL網址
      var url = "getTime.php";
      randNo = parseInt(Math.random()*999999999);
      // 新增亂數避免緩衝區問題
      url = url + "?rand=" + randNo;
      makeRequest(xmlHttp, url); // 建立HTTP請求
   }
   else {
      alert ("錯誤! 瀏覽器不支援XMLHttpRequest物件!");
      return;
   }   
}
// 顯示時間資料
function showTime() {
   if ( xmlHttp.readyState == 4 ) {
      if ( xmlHttp.status == 200 ) {
         // 取得回應
         var xmlResult = xmlHttp.responseXML;
         var myTime;
         // 取得時間
         myTime = xmlResult.getElementsByTagName("now")[0];
         document.getElementById("result").innerHTML = 
                        myTime.childNodes[0].nodeValue;
      }
      else  alert (xmlHttp.status);                 
   }
}