package android.phonegap.ch17;

import org.apache.cordova.*;
import android.os.Bundle;

public class Ch17_5Activity extends DroidGap {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.loadUrl("file:///android_asset/www/index.html");
    }
}